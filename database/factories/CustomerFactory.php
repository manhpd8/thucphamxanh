<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;

$factory->define(App\Models\Customer::class, function () {
    return [
        'firstname' => 'firstname',
        'lastname' => 'lastname',
        'address_id' => rand(1,100),
    ];
});
