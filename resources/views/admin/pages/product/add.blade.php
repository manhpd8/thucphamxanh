@extends('admin.layouts.admin')
@section('content')
	@if(Session::has('success'))
		<p class="alert alert-danger">{{Session::get('success')}}</p>
	@endif
	<h1>Thêm sản phẩm mới</h1>
	<form method="post" enctype="multipart/form-data">{{ csrf_field() }}
		<div class="form-group">
		    <label for="exampleFormControlInput1">Tên sản phẩm</label>
		    <input type="text" class="form-control" name="name" placeholder="rau xà lách">
		</div>
		@if($errors->first('name') != '')
			<p class="alert alert-danger">{{ $errors->first('name') }}</p>
		@endif
		<div class="form-group">
		    <label for="exampleFormControlInput1">Giá</label>
		    <input type="text" class="form-control" name="price" placeholder="156456">
		</div>
		<div class="form-group">
		    <label for="exampleFormControlInput1">Giá cũ</label>
		    <input type="text" class="form-control" name="price_old" placeholder="nên để lớn hơn giá trên">
		</div>
	  	<div class="form-group">
	    	<label for="exampleFormControlSelect1">Danh mục</label>
	    	<select class="form-control" name="parentId">
	    		<option value="0">Không</option>
		      	@foreach($categories as $category)
				<option value="{{$category->parent_id}}">
				{{$category->name}}
				</option>
				@endforeach
	    	</select>
	  	</div>
	  	<div class="form-group">
		    <label for="exampleFormControlInput1">Thương hiệu</label>
		    <input type="text" class="form-control" name="brand_code" placeholder="vietgap">
		</div>
	  	<div class="form-group">
		    <label for="exampleFormControlInput1">Tình trạng</label>
		    <select class="form-control" name="status_code">
		    	<option value="NO">Không có</option>
	    		<option value="NEW">Mới</option>
	    		<option value="SALE">Giảm giá</option>
	    	</select>
		 </div>
	 	<div class="form-group">
	    	<label for="exampleFormControlTextarea1">Mô tả</label>
	    	<textarea class="form-control" name="description" rows="3"></textarea>
	  	</div>
	  	@if($errors->first('content') != '')
	  		<p class="alert alert-danger">{{ $errors->first('content') }}</p>
	  	@endif
	  	<input type="file" name="img">
	  	<button type="submit" class="btn btn-primary" name="add news">Thêm sản phẩm</button>
	</form>
@stop
	