<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use App\Http\Controllers\ProductController;

$factory->define(App\Models\Product::class, function () {
    $name = 'rau sach ha noi'. (rand(1,11000) . rand(11000,19000));
    return [
        'name' => $name,
        'slug' => ProductController::to_slug($name),
        'price' => rand(10000,11000),
        'category_id' => rand(1,10),
        'price_old' => rand(11000,13000),
        'description' => 'Quo voluptates et rerum soluta explicabo numquam et. Esse expedita similique in ut et ut. Beatae provident voluptas aut dolore.',
        'icon' => 'front\images\items\\'. rand(1,7).'.jpg',
        
    ];
});
