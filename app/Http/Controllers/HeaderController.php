<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CategoryController;

class HeaderController extends Controller
{
	protected $categoryController;

    public function __construct(
    	CategoryController $categoryController
    )
    {
    	$this->categoryController = $categoryController;
    	parent::__construct();
    }

    public function index()
    {
    	$amountCategory = 6;
    	$data['categories'] = $this->categoryController->getListCategory($amountCategory);
    	return $data;
    }
}
