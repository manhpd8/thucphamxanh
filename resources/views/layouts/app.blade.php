
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="Thông tin về sản phẩm rau sạch sẽ được cập nhật hàng ngày tại Thực phẩm xanh với những thông tin mới nhất hiện nay trên thị trường Việt Nam"/>
    <link rel="canonical" href="thucphamxanh" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="object" />
    <meta property="og:title" content="Rau sạch - Thực phẩm xanh" />
    <meta property="og:description" content="Thông tin về sản phẩm rau sạch sẽ được cập nhật hàng ngày tại Thực phẩm xanh với những thông tin mới nhất hiện nay trên thị trường Việt Nam" />
    <meta property="og:url" content="thucphamxanh" />
    <meta property="og:site_name" content="Thực phẩm xanh" />
    <meta property="fb:app_id" content="thucphamxanh" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="Thông tin về sản phẩm rau sạch sẽ được cập nhật hàng ngày tại Thực phẩm xanh với những thông tin mới nhất hiện nay trên thị trường Việt Nam" />
    <meta name="twitter:title" content="Rau sạch - Thực phẩm xanh" />
    <meta name="twitter:site" content="@thucphamxanh" />
    <meta name="twitter:image" content="thucphamxanh" />
    <!-- The above 4 meta tags *Must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>@yield('title')</title>

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{ secure_asset('front/images/favicon.png')}}" type="image/x-icon">
    <link rel="icon" href="{{ secure_asset('front/images/favicon.ico')}}" type="image/x-icon">

    <!-- Font awesome 5 -->
    <link href="{{ secure_asset('front/fonts/fontawesome/css/fontawesome-all.min.css')}}" type="text/css" rel="stylesheet">

    <!-- Bootstrap4 files-->
    <link href="{{ secure_asset('front/css/bootstrap.css')}}" rel="stylesheet" type="text/css"/>

    <!-- plugin: owl carousel  -->
    <link href="{{ secure_asset('front/plugins/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{ secure_asset('front/plugins/owlcarousel/assets/owl.theme.default.css')}}" rel="stylesheet">
    

    <!-- custom style -->
    <link href="{{ secure_asset('front/css/ui.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ secure_asset('front/css/responsive.css')}}" rel="stylesheet" media="only screen and (max-width: 1200px)" />


    @yield('styles')
    

</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="spinner"></div>
    </div>
    @include('layouts.header')
    @yield('content')
    

    @include('layouts.footer')

    <!-- jQuery -->
    <script src="{{ secure_asset('front/js/jquery-2.0.0.min.js')}}" type="text/javascript"></script>

   <!-- custom javascript -->
    <script src="{{ secure_asset('front/js/script.js')}}" type="text/javascript"></script>

    <!-- plugin: owl carousel  -->
    <script src="{{ secure_asset('front/plugins/owlcarousel/owl.carousel.min.js')}}"></script>

    <!-- Bootstrap4 files-->
    <script src="{{ secure_asset('front/js/bootstrap.bundle.min.js')}}" type="text/javascript"></script>

    @yield('scripts')
 
</body>

</html>