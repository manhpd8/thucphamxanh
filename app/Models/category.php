<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
class Category extends Model
{
    protected $table = 'categories';

    public function subCategory()
    {
    	return $this->hasMany('App\Models\Category', 'parent_id', 'id');
    }

    public function parCategory()
    {
    	return $this->belongsTo('App\Models\Category', 'id', 'parent_id');
    }
}
