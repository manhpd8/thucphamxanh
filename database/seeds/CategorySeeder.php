<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Rau củ sạch đà lạt' ,
            'level' => 1,
        ]);
        DB::table('categories')->insert([
            'name' => 'Đặc sản vùng miền' ,
            'level' => 1,
        ]);
        DB::table('categories')->insert([
            'name' => 'Đồ ướt' ,
            'level' => 1,
        ]);
        DB::table('categories')->insert([
            'name' => 'Đồ khô' ,
            'level' => 1,
        ]);
        DB::table('categories')->insert([
            'name' => 'Thực phẩm tươi sống' ,
            'level' => 1,
        ]);
        DB::table('categories')->insert([
            'name' => 'Tinh dầu' ,
            'level' => 1,
        ]);
        DB::table('categories')->insert([
            'name' => 'Trà' ,
            'level' => 1,
        ]);
        DB::table('categories')->insert([
            'name' => 'Nấm' ,
            'level' => 1,
        ]);
        DB::table('categories')->insert([
            'name' => 'Hoa quả' ,
            'level' => 1,
        ]);
        DB::table('categories')->insert([
            'name' => 'Hạt giống hoa' ,
            'level' => 1,
        ]);
    }
}
