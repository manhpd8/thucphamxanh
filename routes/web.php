<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','HomeController@index');
Route::get('/gioi-thieu', function () {
    return view('pages.introduction');
});
Route::get('/lien-he', function () {
    return view('pages.contact');
});
Route::get('/quy-trinh-san-xuat', function () {
    return view('pages.introduction');
});

Route::get('/cart', function () {
    return view('pages.cart');
});
Route::get('/search','ProductController@search');

Route::get('/product/{slug}','ProductController@getDetailBySlug');

Route::group(['prefix'=>'admins', 'middleware'=>'checkAdmin'], function(){
    Route::get('/','AdminController@getLogin');
    Route::post('/','AdminController@postLogin');

    Route::group(['prefix'=>'category'],function(){
		Route::get('add','CategoryController@getAdd');
		Route::post('add','CategoryController@postAdd');
		Route::get('edit','CategoryController@getEdit');
		Route::post('edit','CategoryController@postEdit');
		Route::post('del/{cat_id}','CategoryController@postDel');
	});

	Route::group(['prefix'=>'product'],function(){
		Route::get('add','ProductController@getAdd');
		Route::post('add','ProductController@postAdd');
		Route::get('edit','ProductController@getEdit');
		Route::post('edit','ProductController@postEdit');
		Route::post('del/{pro_id}','ProductController@postDel');
	});
});