<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\Status;

use Illuminate\Http\Request;
use DB;
use Validator;
use Session;
use App\Http\Controllers\CategoryController;
class ProductController extends EcommerceController
{
    protected $categoryController;

    public function __construct(
        CategoryController $categoryController
    )
    {
    	parent::__construct();
        $this->categoryController = $categoryController;
    }

    public function index()
    {

    }

    public function store()
    {

    }

    public function update()
    {

    }

    public function delete()
    {

    }

    public function getDetailBySlug($slug)
    {
    	$product = Product::where('slug',$slug)->first();
        if($product == null) {
            return view('pages.404');
        }
        $images = $product->images;
    	$category = $product->category;
    	$likeProduct = $this->getListByCategory($product->category_id, 3);
    	$data['product'] = $product;
        $data['images'] = $images;
    	$data['category'] = $category;
    	$data['likeProduct'] = $likeProduct;
    	
    	return view('pages.product_detail', $data);
    }

    public function getListByTagNew(int $amount = 1)
    {
    	$productsNew = Product::where('status_code',Status::NEW)
    		->orderBy('updated_at', 'desc')
    		->take($amount)
    		->get();
    	return $productsNew;
    }

    public static function to_slug($str) {
	    $str = trim(mb_strtolower($str));
	    $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
	    $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
	    $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
	    $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
	    $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
	    $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
	    $str = preg_replace('/(đ)/', 'd', $str);
	    $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
	    $str = preg_replace('/([\s]+)/', '-', $str);
	    $str = $str . '-' . time();
    	return $str;
	}

	public function getListByCategory($category,$amount)
	{
		$products = Product::where('category_id',$category)
			->orderBy('updated_at', 'desc')
			->take($amount)
			->get();
		return $products;
	}

    public function getAdd(){
        $data['categories'] = DB::table('categories')->get();
        return view('admin.pages.product.add', isset($data)? $data:NULL);
    }

    public function postAdd(Request $request){

        $rules = [
                    'name'=>'required',
                    //'img'=>'required',
                    //'content'=>'required'
        ];

        $messages = [
                    'name.required'=>'tiêu đề bài viết không được để trống',
                    //'img.required'=>'ảnh bài viết không được để trống',
                    //'content.required'=>'nội dung bài viết không được để trống'
        ];

        $Validator = Validator::make($request->all(),$rules,$messages);

        if($Validator->fails()){
            echo "loi validator";
            $errors['errors']=$Validator->errors();
            return redirect()->back()->with($errors);

        }else{
            Session::flash('success','');
            Session::flash('error','');
            $arr['name'] = $request->name;
            $arr['slug'] = $this->to_slug($request->name);
            $arr['category_id'] = $request->cat_id;
            $arr['brand_code']= $request->brand_code;
            $arr['price']= $request->price;
            $arr['price_old']= $request->price_old;
            $arr['description'] = $request->description;
            $arr['status_code'] = $request->status_code;
            $arr['created_at'] = gmdate("Y-m-d H:i:s",time()+7*3600);
            $arr['updated_at'] = gmdate("Y-m-d H:i:s",time()+7*3600);
            $extenImg = strtolower(pathinfo($_FILES['img']['name'],PATHINFO_EXTENSION));
            if($extenImg=='jpg' || $extenImg =='png'){
                $path = 'front/images/items/';
                $arr['icon'] = $path . $_FILES['img']['name'];
                $name ='img';
                $file_name = $request->file($name)->getClientOriginalName();
                // Lưu file vào thư mục upload với tên là biến $filename
                $request->file($name)->move($path,$file_name);
            }

            else{
                Session::flash('error','ảnh không đúng định dạng');
                echo "them loi";
                //return redirect()->back();
            }
            DB::table('products')->insert($arr);
            echo "them thanh cong";
            Session::flash('success','thêm  thành công');
            return redirect()->back();

        }
 
    }

    public function getEdit()
    {
        $products = Product::get();
        $data['categories'] = DB::table('categories')->get();
        $data['products'] = $products;
        return view('admin.pages.product.edit', isset($data)? $data:NULL); 
    }

    public function search(Request $request)
    {
        $keys = (string) $request->keys;
        $status_code = (string) $request->status_code;
        if($status_code != null)
        {
            $products = Product::where('name','like', '%'. $keys .'%')
                ->orwhere('title','like', '%'. $keys .'%')
                ->where('status_code','like', '%'. $status_code .'%');
        } else {
            $products = Product::where('name','like', '%'. $keys .'%');
        }
        $products = $products->paginate(6);
        $data['products'] = $products;
        $data['categories'] = $this->categoryController->getListCategory(6);
        return view('pages.search', $data);
    }
}
