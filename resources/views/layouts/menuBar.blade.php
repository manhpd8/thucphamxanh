<section class="section-main bg padding-y-sm">
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row row-sm">
                    <aside class="col-md-3">
                        <h5 class="text-uppercase title-bg bg-secondary">Danh Mục</h5>
                        <ul class="menu-category" >
                            @foreach($categories as $category)
                            <li ><a href="#" style="font-weight: 500">
                            		<i class="fas fa-angle-double-right" style="margin-right: 3px"></i>
                            		{{$category->name}}
                            		<span class="float-right badge badge-secondary round ">
                            			
                            		</span>
                            </a></li>
                            @endforeach
                            <li class="has-submenu"> 
                            	<a href="#"><i class="fas fa-list-ul" style="margin-right: 5px"></i>Nhiều hơn </a>
                                <ul class="submenu">
                                    <li> <a href="#">Food &amp; Beverage </a></li>
                                </ul>
                            </li>
                        </ul>
                    </aside>
                    <!-- col.// -->
                    <div class="col-md-6">

                        <!-- ================= main slide ================= -->
                        <div class="owl-init slider-main owl-carousel owl-loaded owl-drag" data-items="1" data-nav="true" data-dots="false">

                            <div class="owl-stage-outer">
                                <div class="owl-stage" style="transform: translate3d(-1196px, 0px, 0px); transition: all 0s ease 0s; width: 4186px;">
                                    <div class="owl-item cloned" style="width: 598px;">
                                        <div class="item-slide">
                                            <img src="{{secure_asset('front/images/banners/slide4.jpg')}}">
                                        </div>
                                    </div>
                                    <div class="owl-item cloned" style="width: 598px;">
                                        <div class="item-slide">
                                            <img src="{{secure_asset('front/images/banners/slide5.jpg')}}">
                                        </div>
                                    </div>
                                    <div class="owl-item active" style="width: 598px;">
                                        <div class="item-slide">
                                            <img src="{{secure_asset('front/images/banners/slide6.jpg')}}">
                                        </div>
                                    </div>
                                    <div class="owl-item" style="width: 598px;">
                                        <div class="item-slide">
                                            <img src="{{secure_asset('front/images/banners/slide7.jpg')}}">
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="owl-nav">
                                <button type="button" role="presentation" class="owl-prev"><i class="fa fa-chevron-left"></i></button>
                                <button type="button" role="presentation" class="owl-next"><i class="fa fa-chevron-right"></i></button>
                            </div>
                            <div class="owl-dots disabled"></div>
                        </div>
                        <!-- ============== main slidesow .end // ============= -->

                    </div>
                    <!-- col.// -->
                    <aside class="col-md-3">

                        <h6 class="title-bg bg-secondary"> Qualified Suppliers</h6>
                        <div style="height:280px;">
                            <figure class="itemside has-bg border-bottom" style="height: 33%;">
                                <img class="img-bg" src="{{secure_asset('front/images/items/item-sm.png')}}">
                                <figcaption class="p-2">
                                    <h6 class="title">One request, many offers </h6>
                                    <a href="#">View more</a>
                                </figcaption>
                            </figure>

                            <figure class="itemside has-bg border-bottom" style="height: 33%;">
                                <img class="img-bg" src="{{secure_asset('front/images/items/1.jpg')}}" height="80">
                                <figcaption class="p-2">
                                    <h6 class="title">One request, many offers </h6>
                                    <a href="#">View more</a>
                                </figcaption>
                            </figure>
                            <figure class="itemside has-bg border-bottom" style="height: 33%;">
                                <img class="img-bg" src="{{secure_asset('front/images/items/2.jpg')}}" height="80">
                                <figcaption class="p-2">
                                    <h6 class="title">One request, many offers </h6>
                                    <a href="#">View more</a>
                                </figcaption>
                            </figure>
                        </div>

                    </aside>
                </div>
                <!-- row.// -->
            </div>
            <!-- card-body .// -->
        </div>
        <!-- card.// -->

    </div>
    <!-- container .//  -->
</section>