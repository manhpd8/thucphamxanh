<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use DB;
use Validator;
use Session;
class CategoryController extends Controller
{
    public function __construct()
    {
    	parent::__construct();
    }

    public function index()
    {
    	
    }

    public function store()
    {
    	
    }

    public function update()
    {

    }

    public function delete()
    {

    }

    public function getListCategory($amount=1)
    {
    	$categories = Category::where('level','>',0)
    		->orderBy('level', 'asc')
    		->take($amount)
    		->get();
    	return $categories;
    }

    public function getSubCategory($parentId, $amount=1)
    {
        $categories = Category::where('level','>',0)
            ->orderBy('parent_id', 'parentId')
            ->take($amount)
            ->get();
        return $categories;
    }

    public function getCategoryById($id = 1)
    {
        $category = Category::where('id',$id)
            ->first();
        return $category;
    }

    public function getAdd(){
        $data['categories'] = DB::table('categories')->distinct()->get();
        //dd($data);
        return view('admin.pages.category.add',isset($data)? $data:NULL);
    }

    public function postAdd(Request $request){
        $rules =[
            'name' =>"required",
        ];
        $messages = [
            'name.required' =>'ten category khong duoc de trong',
        ];
        $Validator = Validator::make($request->all(),$rules,$messages);

        if($Validator->fails()){
            echo "loi Validator";
            $errors['errors']=$Validator->errors();
            return redirect()->back()->with($errors);
        }else{
            Session::flash('success','');
            Session::flash('error','');
            $arr['name'] = $request->name;
            $arr['description'] = $request->description;
            $arr['level'] = $request->level;
            $arr['parent_id'] = $request->parentId;
            $arr['created_at'] = gmdate("Y-m-d H:i:s",time()+7*3600);
            $arr['updated_at'] = gmdate("Y-m-d H:i:s",time()+7*3600);
            DB::table('categories')->insert($arr);
            echo "them thanh cong";
            $errors['success'] = 'thêm category thành công';
            Session::flash('success','thêm category thành công');
            return redirect()->back();
        }
    }

    public function getEdit(){
        $data['categories'] = DB::table('categories')->get();
        return view('admin.pages.category.edit',$data);
    }
    public function postEdit(Request $request){
        $rules =[
            'name' =>"required",
        ];
        $messages = [
            'name.required' =>'ten category khong duoc de trong',
        ];
        $Validator = Validator::make($request->all(),$rules,$messages);

        if($Validator->fails()){
            echo "loi Validator";
            $errors['errors']=$Validator->errors();
            return redirect()->back()->with($errors);
        }else{
            Session::flash('success','');
            Session::flash('error','');
            $cat_id = $request->id;
            $arr['name'] = $request->name;
            $arr['parent_id'] = $request->parentId;
            $arr['description'] = $request->description;
            $arr['level'] = $request->level;
            $arr['updated_at'] = gmdate("Y-m-d H:i:s",time()+7*3600);
            DB::table('categories')->where('id',$cat_id)->update($arr);
            echo "sửa thanh cong";
            Session::flash('success','Sửa category thành công');
            return redirect()->back();
        }
    }

    public function postDel($cat_id){
        DB::table('categories')->where('id',$cat_id)->delete();
        Session::flash('success','xoa thành công category');
        return redirect()->back();
    }

}
