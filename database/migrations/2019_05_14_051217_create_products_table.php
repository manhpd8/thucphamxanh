<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
            $table->string('title')->nullable();
            $table->integer('price');
            $table->integer('price_old')->nullable();
            $table->integer('category_id')->nullable();
            $table->char('brand_code',10)->nullable();
            $table->text('description')->nullable();
            $table->string('slug')->index()->nullable();
            $table->char('status_code',10)->index()->nullable();
            $table->string('icon')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
