<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HeaderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;

class HomeController extends EcommerceController
{
	protected $headerController;
	protected $productController;
	protected $categoryController;

    public function __construct(
    	HeaderController $headerController,
    	ProductController $productController,
    	CategoryController $categoryController
    )
    {
    	$this->headerController = $headerController;
    	$this->productController = $productController;
    	$this->categoryController = $categoryController;

    	parent::__construct();
    }

    public function index()
    {
    	$data['categories'] = $this->categoryController->getListCategory(6);
    	$data['productsNew'] = $this->productController->getListByTagNew(4);
        $data['cat1'] = $this->categoryController->getCategoryById(1);
    	$data['pros_cat1'] = $this->productController->getListByCategory(1,8);
        $data['cat2'] = $this->categoryController->getCategoryById(2);
        $data['pros_cat2'] = $this->productController->getListByCategory(2,4);
        //dd($data);
    	return view('pages.home',$data);
    }

    public function store()
    {
    	
    }

    public function update()
    {

    }

    public function delete()
    {

    }
}
