@extends('admin.layouts.admin')
@section('content')

        <div class="panel panel-default">
          <!-- Default panel contents -->
          @if($errors->first('name') != null)
            <p class="alert alert-danger">{{ $errors->first('name') }}</p>
          @endif
          @if(Session::has('success'))
            <p class="alert alert-danger">{{Session::get('success')}}</p>
          @endif
          <h1>Sửa danh mục</h1>
          <div class="panel-body">
            <table class="table" id="table_content">
              <thead class="thead-dark">
                <th scope="col">id</th>
                <th scope="col">icon</th>
                <th scope="col">Tên</th>
                <th scope="col">Tiêu đề</th>
                <th scope="col">Giá</th>
                <th scope="col">Danh mục</th>
                <th scope="col">Brand code</th>
                <th scope="col">Trạng thái</th>
                <th scope="col">Cập nhật</th>
                <th scope="col">action</th>
            </thead>
            @foreach($products as $item)
              <tr>
                <td>{{$item->id}}</td>
                <td><img src="{{secure_asset($item->icon)}}" style="width: 33px; height: 33px"></td>
                <td><p  id="category_name">{{$item->name}}</p></td>
                <td><p  id="description">{{$item->title}}</p></td>
                <td><p  id="description">{{$item->price}}</p></td>
                <td><p  id="level">{{$item->category->name}}</p></td>
                <td><p  id="status_code">{{$item->brand_code}}</p></td>

                <td><p  id="status_code">{{$item->status_code}}</p></td>
                <td><p  id="updated_at">{{$item->updated_at}}</p></td>
                <td>
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter{{$item->id}}">Edit</button>
                  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirm-del{{$item->id}}">Delete</button>
                </td>
              </tr>
              <!-- modal -->
              <div class="modal fade" id="exampleModalCenter{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Id danh mục = {{$item->id}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <!-- form edit -->
                    <form method="post">{{ csrf_field() }}
                      <input type="" name="id" value="{{$item->id}}" hidden="true">
                      <div class="form-group">
                          <label for="exampleFormControlInput1">Tên danh mục</label>
                          <input type="text" class="form-control" name="name" placeholder="tên danh mục" value="{{$item->name}}">
                      </div>
                      <div class="form-group">
                        <label for="exampleFormControlSelect1">Danh mục</label>
                        <select name="parent_id">
                          <option value="0">None</option>
                          @foreach($categories as $item2)
                            @if($item2->id == $item->parent_id)
                              <option value="{{$item2->id}}" selected="">{{$item2->name}}</option>
                            @else
                              <option value="{{$item2->id}}">{{$item2->name}}</option>
                            @endif
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleFormControlInput1">Mô tả</label>
                          <textarea type="text" class="form-control" name="description" placeholder="mô tả">{{$item->description}}</textarea>
                      </div>
                      <div class="form-group">
                        <label for="exampleFormControlSelect1">Ưu tiên hiển thị (1 là cao nhất) </label>
                        <select class="form-control" name="level">
                          <?php  
                            for($i=1;$i<10;$i++){
                              echo '<option value="'. $i .'">'. $i .'</option>';
                            }
                          ?>
                        </select>
                      </div>  
                      <!-- <input type="file" name="img"> -->
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                      </div>
                    </form>

                  </div>
                  
                </div>
              </div>
            </div><!--  end modal -->
            <!-- confirm delete -->
            <div class="modal fade" id="confirm-del{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">Confirm Delete Post </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <form action="{{secure_asset('category/del/'.$item->id)}}" method="post">{{ csrf_field() }}
                  <div class="modal-body">
                      {{$item->name}}
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submmit" class="btn btn-danger" >Delete</button>
                  </div>
                </form>
                </div>
              </div>
            </div>
            <!-- end confirm delete -->
            </div>
            @endforeach
          </table>
          </div>
        </div>
@stop
  <!-- Demo scripts for this page-->
