<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use Session;
class AdminController extends Controller
{
    public function getLogin(){
		return view('admin.pages.login');
	}
	public function postLogin(Request $request){
		$rules = [
			'user'=>'required',
			'pass'=>'required',
		];
		$messages = [
			'user.required'=>'tài khoản không được để trống',

			'pass.required'=>'mật khẩu không được để trống',
		];
		$Validator = Validator::make($request->all(),$rules,$messages);
		// input
		$name =  $request->input('user');
		$pass = $request->input('pass');
		if($Validator->fails()){
			$errors['errors']=$Validator->errors();
			return response()->view('admin.pages.login',$errors);
			
		}
		else{
			$name =  $request->input('user');
			$pass = $request->input('pass');
			$path = $request->path();
			$url = $request->url();
			#$fullurl = $requet->fullurl();
			$method = $request->method();
			$coditions = [
				'name'=>$name,
				'password'=>$pass
			];
			// check db
			if(DB::table('users')->where($coditions)->count() > 0){
				// luu cookie nguoi dung
				$minutes =200;
				$cookie = cookie('cookie_user', $name, $minutes);
				//luu session
				$data = DB::table('users')->where($coditions)->first();
				Session::put('login',$data);
				$errors['success']='dang nhap thanh cong';
				//return redirect()->action('HomeController@getHome');
				//return response()->view('home');//->withCookie($cookie);
				return view('admin.pages.index');

			} else{
				Session::flash('error','sai tai khoan mat khau');
				return response()->view('admin.pages.login');
			}
		}
	}

	public function getLogout(){
		Session::flush();

		return redirect()->intended('login');
	}

}
