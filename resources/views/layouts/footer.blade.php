<!-- ========================= SECTION LINKS ========================= -->
<section class="section-links bg padding-y-sm" >
<div class="container">
<div class="row">
	<div class="col-sm-6">
<header class="section-heading heading-line">
	<h4 class="title-section bg text-uppercase">Đối tác của chúng tôi</h4>
</header>

<ul class="list-icon row">
	<li class="col-md-4"><a href="#"><img src="{{ secure_asset('front/images/icons/flag-usa.png')}}"><span>United States</span></a></li>
	<li class="col-md-4"><a href="#"><img src="{{ secure_asset('front/images/icons/flag-in.png')}}"><span>India</span></a></li>
	<li class="col-md-4"><a href="#"><img src="{{ secure_asset('front/images/icons/flag-tr.png')}}"><span>Turkey</span></a></li>
	<li class="col-md-4"><a href="#"><img src="{{ secure_asset('front/images/icons/flag-kr.png')}}"><span>Korea</span></a></li>
	<li class="col-md-4"><a href="#"><img src="{{ secure_asset('front/images/icons/flag-tr.png')}}"><span>Turkey</span></a></li>
	<li class="col-md-4"><a href="#"><img src="{{ secure_asset('front/images/icons/flag-kr.png')}}"><span>Korea</span></a></li>
</ul>
	</div> <!-- col // -->

	<div class="col-sm-6">
<header class="section-heading heading-line">
	<h4 class="title-section bg text-uppercase">các dịch vụ cung cấp</h4>
</header>
<ul class="list-icon row">
	<li class="col-md-4"><a href="#"><i class="icon fa fa-comment"></i><span>Trade Assistance</span></a></li>
	<li class="col-md-4"><a href="#"><i class="icon  fa fa-suitcase"></i><span>Business Identity</span></a></li>
	<li class="col-md-4"><a href="#"><i class="icon fa fa-globe"></i><span>Worldwide delivery</span></a></li>
	<li class="col-md-4"><a href="#"><i class="icon fa fa-phone-square"></i><span>Customer support</span></a></li>
	<li class="col-md-4"><a href="#"><i class="icon fa fa-globe"></i><span>Worldwide delivery</span></a></li>
	<li class="col-md-4"><a href="#"><i class="icon fa fa-phone-square"></i><span>Customer support</span></a></li>
</ul>
	</div> <!-- col // -->
</div><!-- row // -->

<!-- <figure class="mt-3 banner p-3 bg-secondary">
	<div class="text-lg text-center white">Another banner can be here</div>
</figure> -->

</div><!-- container // -->
</section>
<!-- ========================= SECTION LINKS END.// ========================= -->

<!-- ========================= FOOTER ========================= -->
<footer class="section-footer bg-secondary" style="background-color: #155415  !important">
	<div class="container">
		<section class="footer-top padding-top">
			<div class="row">
				<aside class="col-sm-3 col-md-3 white">
					<h5 class="title">Chăm sóc khách hàng</h5>
					<ul class="list-unstyled">
						<li> <a href="#">Trung tâm hỗ trợ</a></li>
						<li> <a href="#">Hoàn trả</a></li>
						<li> <a href="#">Điề khoản & chính sách</a></li>
						<li> <a href="#">Open dispute</a></li>
					</ul>
				</aside>
				<aside class="col-sm-3  col-md-3 white">
					<h5 class="title">Tài khoản</h5>
					<ul class="list-unstyled">
						<li> <a href="#"> Đăng nhập </a></li>
						<li> <a href="#"> Đăng ký </a></li>
						<li> <a href="#"> Cài đặt </a></li>
						<li> <a href="#"> Đơn hàng </a></li>
						<li> <a href="#"> Yêu thích </a></li>
					</ul>
				</aside>
				<aside class="col-sm-3  col-md-3 white">
					<h5 class="title">Chúng tôi</h5>
					<ul class="list-unstyled">
						<li> <a href="#"> Lịch sử </a></li>
						<li> <a href="#"> Mua hàng </a></li>
						<li> <a href="#"> Vận chuyển và thanh toán </a></li>
						<li> <a href="#"> Quảng cáo </a></li>
						<li> <a href="#"> Đối tác </a></li>
					</ul>
				</aside>
				<aside class="col-sm-3">
					<article class="white">
						<h5 class="title">Liên hệ</h5>
						<p>
							<strong>Điện thoại: </strong> +123456789 <br> 
						    <strong>Email:</strong> thucphamxanh@gmail.com
						</p>

						 <div class="btn-group white">
						    <a class="btn btn-facebook" title="Facebook" target="_blank" href="#"><i class="fab fa-facebook-f  fa-fw"></i></a>
						    <a class="btn btn-instagram" title="Instagram" target="_blank" href="#"><i class="fab fa-instagram  fa-fw"></i></a>
						    <a class="btn btn-youtube" title="Youtube" target="_blank" href="#"><i class="fab fa-youtube  fa-fw"></i></a>
						    <a class="btn btn-twitter" title="Twitter" target="_blank" href="#"><i class="fab fa-twitter  fa-fw"></i></a>
						</div>
					</article>
				</aside>
			</div> <!-- row.// -->
			<br> 
		</section>
		<section class="footer-bottom row border-top-white">
			<div class="col-sm-6"> 
				<p class="text-white-50"> Made with <br>  by Adam Kyle</p>
			</div>
			<div class="col-sm-6">
				<p class="text-md-right text-white-50">
					Copyright &copy  <br>
					<a href="" class="text-white-50">Bootstrap</a>
				</p>
			</div>
		</section> <!-- //footer-top -->
	</div><!-- //container -->
</footer>