@extends('layouts.app')
@section('title', "Thực phẩm xanh")
@section('styles')

@endsection
@section('content')
@include('layouts.menuBar')
<section class="section-content padding-y-sm bg">
<div class="container">
	<header class="section-heading heading-line">
		<h4 class="title-section bg text-uppercase ">{{$cat1->name}}</h4>
	</header>
	<div class="row">
		@foreach($pros_cat1 as $pro_cat1)
		<div class="col-md-3">
			<figure class="card card-product">
				@switch($pro_cat1->status_code)
				    @case('NEW')
				        <span class="badge-new"> NEW </span>
				        @break

				    @case('SALE')
				        <span class="badge-offer"><b> - 50%</b></span>
				        @break

				    @default
				        
				@endswitch
				<div class="img-wrap"> 
					<img src="{{secure_asset($pro_cat1->icon)}}">
					<a class="btn-overlay" href="{{secure_asset('product/'.$pro_cat1->slug)}}"><i class="fa fa-search-plus"></i> Xem ngay</a>
				</div>
				<figcaption class="info-wrap">
					<a href="product/{{$pro_cat1->slug}}" class="title" style="color: #155415; font-weight: 700;
											 font-size: 23px;">{{substr($pro_cat1->name,0,21)}}</a>
					<div class="action-wrap">
						<a href="#" class="btn  btn-outline-success float-right"> <i class="fas fa-shopping-cart"></i></a>
						<div class="price-wrap h5">
							<span class="price-new" style="color: #FF0100; font-weight: 700;">{{number_format($pro_cat1->price)}} vnd/kg</span>
							<del class="price-old" style="]color: #627F9A">{{number_format($pro_cat1->price_old)}} vnd</del>
						</div> <!-- price-wrap.// -->
					</div> <!-- action-wrap -->
				</figcaption>
			</figure> <!-- card // -->
		</div> <!-- col // -->
		@endforeach
	</div> <!-- row.// -->
</div>
</section>

<section class="section-content padding-y-sm bg">
<div class="container">
	<header class="section-heading heading-line">
		<h4 class="title-section bg text-uppercase ">{{$cat2->name}}</h4>
	</header>
	<div class="row">
		@foreach($pros_cat2 as $pro_cat2)
		<div class="col-md-3">
			<figure class="card card-product">
				@switch($pro_cat2->status_code)
				    @case('NEW')
				        <span class="badge-new"> NEW </span>
				        @break

				    @case('SALE')
				        <span class="badge-offer"><b> - 50%</b></span>
				        @break

				    @default
				        
				@endswitch
				<div class="img-wrap"> 
					<img src="{{secure_asset($pro_cat2->icon)}}">
					<a class="btn-overlay" href="{{secure_asset('product/'.$pro_cat2->slug)}}"><i class="fa fa-search-plus"></i> Xem ngay</a>
				</div>
				<figcaption class="info-wrap">
					<a href="product/{{$pro_cat2->slug}}" class="title" style="color: #155415; font-weight: 700;
											 font-size: 23px;">{{substr($pro_cat2->name,0,21)}}</a>
					<div class="action-wrap">
						<a href="#" class="btn  btn-outline-success float-right"> <i class="fas fa-shopping-cart"></i></a>
						<div class="price-wrap h5">
							<span class="price-new" style="color: #FF0100; font-weight: 700;">{{number_format($pro_cat2->price)}} vnd/kg</span>
							<del class="price-old" style="]color: #627F9A">{{number_format($pro_cat2->price_old)}} vnd</del>
						</div> <!-- price-wrap.// -->
					</div> <!-- action-wrap -->
				</figcaption>
			</figure> <!-- card // -->
		</div> <!-- col // -->
		@endforeach
	</div> <!-- row.// -->
</div>
</section>
@endsection
@section('scripts')

@endsection
