<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use DB;
class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::has('login')){
            $coditions = [
                'name'=>Session::get('login')->name,
                'password'=>Session::get('login')->password
            ];
            if(DB::table('users')->where($coditions)->count() ==1){
                return $next($request);
            }
            else{
                
                return view('admin.pages.login');
            }
        }
        else{
            return $next($request);
        }
    }
}
