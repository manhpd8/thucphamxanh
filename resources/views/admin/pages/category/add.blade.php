@extends('admin.layouts.admin')
@section('content')
	@if(Session::has('success'))
		<p class="alert alert-danger">{{Session::get('success')}}</p>
	@endif
	<h1>Thêm danh mục</h1>
	<form method="post">{{ csrf_field() }}
		<div class="form-group">
		    <label for="exampleFormControlInput1">Tên danh mục</label>
		    <input type="text" class="form-control" name="name" placeholder="rau củ">
		</div>
		@if($errors->first('name') != '')
			<p class="alert alert-danger">{{ $errors->first('name') }}</p>
	  	@endif 
	  	<div class="form-group">
	    	<label for="exampleFormControlSelect1">Thuộc về danh mục khác: </label>
	    	<select class="form-control" name="parentId">
	    		<option value="0">Không</option>

		      	@foreach($categories as $category)
				<option value="{{$category->parent_id}}">
				{{$category->name}}
				</option>
				@endforeach
	    	</select>
	  	</div>
	  	<div class="form-group">
	    	<label for="exampleFormControlSelect1">Ưu tiên hiển thị (1 là cao nhất) </label>
	    	<select class="form-control" name="level">
	    		<?php  
	    			for($i=1;$i<10;$i++){
	    				echo '<option value="'. $i .'">'. $i .'</option>';
	    			}
	    		?>
	    	</select>
	  	</div>
	  	<div class="form-group">
		    <label for="exampleFormControlInput1">Mô tả</label>
		    <textarea type="text" class="form-control" name="description" placeholder="mô tả" style="height: 15rem"></textarea>
		</div>
	  	<button type="submit" class="btn btn-primary" name="add news">Thêm danh mục</button>
	</form>
@stop
	